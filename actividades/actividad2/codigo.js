var btn_1 = document.getElementById("btn-1");
var btn_2 = document.getElementById("btn-2");
var btn_3 = document.getElementById("btn-3");
var btn_4 = document.getElementById("btn-4");

var existe = Boolean;
var invalido = Boolean;
var entero = Boolean;

var resultado = document.getElementById("resultado")
var resultado_invalido = document.getElementById("resultado_invalido")

var inputUno = document.getElementById("input-uno");
var inputDos = document.getElementById("input-dos");

function validarExiste(n1, n2) {
    var n1 = inputUno.value;
    var n2 = inputDos.value;
    // si n1 y n2 no estan vacios retorna true
    if (n1 != "" && n2 != "") {
        return true;
    } else {
        //si los campos estan vacios retorna false
        resultado.innerHTML = ("");
        resultado_invalido.innerHTML = ("campos vacios");
        return false;
    }
}

function validarNumero(n1, n2) {
    var n1 = inputUno.value;
    var n2 = inputDos.value;
    var X = Boolean;
    var Y = Boolean;
    X = !isNaN(n1);
    Y = !isNaN(n2);
    // si n1 y n2 son numeros retorna true
    if (X == true && Y == true) {
        return true;
    } else {
        // si n1 y n2 no son numeros retorna false
        resultado.innerHTML = ("");
        resultado_invalido.innerHTML = ("no es numero");
        return false;
    }
}

function validarEntero(n1, n2) {
    var n1 = inputUno.value;
    var n2 = inputDos.value;
    // si n1 y n2 son numeros retorna true
    if (n1 % 1 == 0 && n2 % 1 == 0) {
        return true;
        // si n1 y n2 son texto retorna false
    } else {
        resultado_invalido.innerHTML = ("no es numero entero");
        return false;
    }
}


btn_1.addEventListener("click", function() {
    var n1 = inputUno.value;
    var n2 = inputDos.value;
    resultado_invalido.innerHTML = ("");
    resultado.innerHTML = suma(n1, n2);
});

function suma(n1, n2) {
    existe = validarExiste(n1, n2);
    if (existe == true) {
        invalido = validarNumero(n1, n2);
        if (invalido == true) {
            entero = validarEntero(n1, n2);
            if (entero == true) {
                return parseInt(n1) + parseInt(n2);
            }
            return resultado.innerHTML = ("");
        }
        return resultado.innerHTML = ("");
    } else {
        return resultado.innerHTML = ("");
    }
}

btn_2.addEventListener("click", function() {
    var n1 = inputUno.value;
    var n2 = inputDos.value;
    resultado_invalido.innerHTML = ("");
    resultado.innerHTML = resta(n1, n2);
});

function resta(n1, n2) {
    {
        existe = validarExiste(n1, n2);
        if (existe == true) {
            invalido = validarNumero(n1, n2);
            if (invalido == true) {
                entero = validarEntero(n1, n2);
                if (entero == true) {
                    return parseInt(n1) - parseInt(n2);
                }
                return resultado.innerHTML = ("");
            }
            return resultado.innerHTML = ("");
        } else {
            return resultado.innerHTML = ("");
        }
    }
}

btn_3.addEventListener("click", function() {
    var n1 = inputUno.value;
    var n2 = inputDos.value;
    resultado_invalido.innerHTML = ("");
    resultado.innerHTML = multiplicacion(n1, n2);

});

function multiplicacion(n1, n2) {
    {
        existe = validarExiste(n1, n2);
        if (existe == true) {
            invalido = validarNumero(n1, n2);
            if (invalido == true) {
                entero = validarEntero(n1, n2);
                if (entero == true) {
                    return parseInt(n1) * parseInt(n2);
                }
                return resultado.innerHTML = ("");
            }
            return resultado.innerHTML = ("");
        } else {
            return resultado.innerHTML = ("");
        }
    }
}

btn_4.addEventListener("click", function() {
    var n1 = inputUno.value;
    var n2 = inputDos.value;
    if (parseInt(n2) == 0) {
        resultado.innerHTML = ("");
        resultado_invalido.innerHTML = ("division por 0 invalida");

    } else {
        resultado.innerHTML = division(n1, n2);
    }
});

function division(n1, n2) {
    {
        existe = validarExiste(n1, n2);
        if (existe == true) {
            invalido = validarNumero(n1, n2);
            if (invalido == true) {
                entero = validarEntero(n1, n2);
                if (entero == true) {
                    return parseInt(n1) / parseInt(n2);
                }
                return resultado.innerHTML = ("");
            }
            return resultado.innerHTML = ("");
        } else {
            return resultado.innerHTML = ("");
        }
    }
}